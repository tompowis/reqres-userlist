from flask import Flask, redirect, url_for, request, render_template
from flask_assets import Environment, Bundle
from services import ReqresApi, ObjectMapper
from models.Pages import Pagination

app = Flask(__name__)

assets = Environment(app)
assets.url = app.static_url_path
scss = Bundle('scss/all.scss', filters='pyscss', output='all.css')
assets.register('scss_all', scss)

@app.route("/")
def index():
    return redirect(url_for('users'))

@app.route('/<path:dummy>')
def fallback(dummy):
    return redirect(url_for('users'))

@app.route("/users")
def users():

    page = request.args.get('page', default = 1, type = int)
    response = ReqresApi().users(page)
    users = ObjectMapper(response)

    pages = Pagination(page, response['per_page'], response['total_pages'])

    return render_template('users.html', users=users, pages=pages)

if __name__ == "__main__":
    app.run(debug=True)
