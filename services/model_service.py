from models import User

def obj_converter(obj):
    for i in obj['data']:
        yield User(**i)
