__all__ = [
    "ObjectMapper",
    "ReqresApi"
]

from .model_service import obj_converter as ObjectMapper
from .reqres_api import ReqresApi