import requests

class ReqresApi(object):

    _URL = 'https://reqres.in/'
    _Host = lambda self, m : ReqresApi._URL + m

    _options = {
        "per_page" : 3
    }


    def users(self, page):
        r = requests.get(
            url    = self._Host('api/users'),
            params = {
                'page' : page,
                **(self._options)
            }
        )
        return r.json()