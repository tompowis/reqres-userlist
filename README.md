# Reqres-UserList

Usage:

1. `pip install -r requirements.txt`
2. `export FLASK_APP=app.py`
3. `flask run` or `flask run --port XXXX`

or

1. `pip install -r requirements.txt`
2. `python app.py`


### Framework Choice

I had not yet had a chance to use flask so i thought this would be a good opportunity to learn it and give it a go.

> Not tested on Windows
