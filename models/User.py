class User(object):

    def __init__(self, id, email, first_name, last_name, avatar):
        self.id = id
        self.email = email
        self.firstName = first_name
        self.lastName = last_name
        self.avatar = avatar

    def __repr__(self):
        return type(self).__name__ + str(tuple(self.__dict__.values()))
