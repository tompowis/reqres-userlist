from math import ceil

class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count
        
    def __repr__(self):
        return type(self).__name__ + str(tuple(self.__dict__.values()))

    def next_page(self):
        return int(self.page + 1 if self.has_next else self.page)

    def prev_page(self):
        return self.page - 1 if self.has_prev else self.page

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.total_count
